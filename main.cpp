#include <openssl/md5.h>

#include <array>
#include <chrono>
#include <cstdint>
#include <iostream>
#include <regex>
#include <string>


using namespace std::chrono;
using namespace std;


// =========================== //
// === Program parameters. === //
// =========================== //

// Regex of solution.
static auto constexpr solution_regex = "\'(\\|\\||or)\'[1-9]";

// Seeds of xoroshift+128 algorithm used as primary PRNG.
// Values taken from jump() function provided with xoroshift+128.
// See http://xoroshiro.di.unimi.it/xoroshiro128plus.c.
static auto constexpr seed1 = 0xbeac0467eba5facb;
static auto constexpr seed2 = 0xd86b048b86aa9922;

// Size of randomly generated strings input to hash algorithm.
static auto constexpr input_size = 32;

// String of valid characters. Order affects randomization.
static constexpr decltype(auto) valid = "0123456789abcdefghijklmnopqrstuvwxyz";


static auto random_string() -> unsigned char const*;


int main() {
    ios::sync_with_stdio(false);


    // Used to indicate duration of program execution.
    auto const start = steady_clock::now();


    while (true) {
        static auto constexpr hash_length = MD5_DIGEST_LENGTH;

        // Input to MD5 hash.
        auto const input = random_string();

        // Output of MD5 hash.
        auto const hash = [&]() {
            // +1 for null terminator.
            static auto hash = array<unsigned char, hash_length + 1>{{}};

            MD5(input, input_size, hash.data());

            return hash.data();
        }();


        static auto const show_characters = [](unsigned char const* hash) {
            for (auto i = 0; i < hash_length; ++i) {
                if (isgraph(hash[i])) cout << hash[i];
                else cout << ' ';
            }
        };


        static auto attempts = 0u;


        static auto const show_attempt = [&] {
            cout << "Attempt " << attempts << ": " << input << '\t';
            show_characters(hash);
            cout << endl;
        };


        // Used to keep track of when to print progress.
        static auto timer = steady_clock::now();


        ++attempts; // This is a new attempt!


        // Print progress every second.
        if (duration_cast<seconds>(steady_clock::now() - timer).count()) {
            show_attempt();

            timer = steady_clock::now();
        }


        static auto const solution = regex(
            solution_regex,
            regex_constants::icase          // Case doesn't matter.
            | regex_constants::optimize     // Generate DFA.
            | regex_constants::ECMAScript   // Using ECMAScript syntax.
        );


        // Check if input gives a solution.
        if (regex_search(reinterpret_cast<char const*>(hash), solution)) {
            show_attempt();

            cout << "\nSolution:   " << input << '\n';
            cout << "Time:       "
                 << duration_cast<
                        duration<double, seconds::period>
                    >(steady_clock::now() - start).count()
                 << " seconds" << '\n';
            cout << "Characters: "; show_characters(hash); cout << '\n';

            break; // Found a solution! End loop.
        }
    }
}


// Source: http://xoroshiro.di.unimi.it; modified for personal use.
static auto xoroshift128plus() -> uint_fast64_t {
    static uint_fast64_t s[2] = {seed1, seed2};

    static auto const rotl = [](uint_fast64_t const x, int k) {
        return (x << k) | (x >> (64 - k));
    };

    uint_fast64_t const s0 = s[0];
    uint_fast64_t       s1 = s[1];
    uint_fast64_t const result = s0 + s1;


    s1 ^= s0;
    s[0] = rotl(s0, 55) ^ s1 ^ (s1 << 14);
    s[1] = rotl(s1, 36);


    return result;
}


static auto random_string() -> unsigned char const* {
    static auto constexpr size  = sizeof(valid) - 1;
    static auto constexpr attempts = input_size;

    // +1 for null terminator.
    static auto result = array<unsigned char, attempts + 1>{{}};


    for (auto i = 0; i < attempts; ++i) {
        result[i] = valid[xoroshift128plus() % size];
    }


    return result.data();
}
