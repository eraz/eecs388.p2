### General C/C++ Makefile written by eraz.
###
### Written to work with most simple C/C++ starting code without hassle.
### Feel free to copy, modify, or share at will. Credit if you so desire.

EXE = main
SRC = cpp
RTA =

# Compilation settings.
CXX        = g++
CXXVERSION = -std=c++14
CXXFLAGS   = -Wall -Wextra -pedantic-errors
LFLAGS     = -lcrypto
LDFLAGS    =

# Build configurations.
all: $(EXE)

release: CXXFLAGS += -Ofast -DNDEBUG
release: all

debug: CXXFLAGS += -g3 -DDEBUG
debug: all

profile: CXXFLAGS += -pg
profile: all

run: release
	./$(EXE) $(RTA)

check: debug
	valgrind -q --leak-check=full ./$(EXE) $(RTA)

time: profile
	./$(EXE) $(RTA) && gprof -bq $(EXE) gmon.out | vim -R -

# File and dependency parser.
SOURCES := $(wildcard *.$(SRC))
OBJECTS := $(SOURCES:%.$(SRC)=%.o)
DEPS    := $(OBJECTS:%.o=%.d)
ifneq ($(MAKECMDGOALS),clean)
sinclude $(DEPS)
endif

# Compilation commands.
$(EXE): $(OBJECTS)
	$(CXX) $(CXXVERSION) $(CXXFLAGS) -o $(EXE) $(OBJECTS) $(LDFLAGS) $(LFLAGS)
	@command -v cppcheck > /dev/null 2>&1 && \
		if [ ! -f .suppressions ]; then \
			cppcheck -q --enable=all --suppress=missingIncludeSystem \
				--suppress=unmatchedSuppression . || exit 0; \
		else \
			cppcheck -q --enable=all --suppressions-list=.suppressions . \
			|| exit 0; \
		fi

%.o: %.$(SRC) Makefile
	$(CXX) $(CXXVERSION) $(CXXFLAGS) -MP -MMD -o $@ -c $< $(LDFLAGS)

clean:
	rm -f $(EXE) *.o *.d vgcore.* gmon.out

# No output files.
.PHONY: all release debug profile time clean

# Disable built-in rules.
.SUFFIXES:
